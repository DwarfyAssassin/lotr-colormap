package io.gitlab.dwarfyassassin;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.logging.log4j.Logger;

import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.biome.variant.LOTRBiomeVariantList;

public class ColorMapUtils {
    private static Logger log = LOTRColorMap.getLogger();
    
    public static List<LOTRBiome> removeNullFromArray(LOTRBiome[] biomeArr) {
        List<LOTRBiome> biomeList = new ArrayList<LOTRBiome>();
        for(LOTRBiome biome : biomeArr) {
            if(biome != null) biomeList.add(biome);
        }
        return biomeList;
    }
    
    public static Set<LOTRBiomeVariant> getBiomeVariants(LOTRBiome biome) {
        LOTRBiomeVariantList[] variantsLists = {biome.getBiomeVariantsLarge(), biome.getBiomeVariantsSmall()};
        Set<LOTRBiomeVariant> allVariants = new HashSet<LOTRBiomeVariant>();
        
        try {
            for(LOTRBiomeVariantList variantList : variantsLists) {
                Field variantListField = variantList.getClass().getDeclaredField("variantList");
                variantListField.setAccessible(true);
                List<?> variantBucketList = (List<?>) variantListField.get(variantList);
                
                if(variantBucketList.isEmpty()) continue;
        
                for(Object variantBucket :variantBucketList) {
                    Field biomeVariantField = variantBucket.getClass().getDeclaredField("variant");
                    biomeVariantField.setAccessible(true);
                    LOTRBiomeVariant biomeVariant = (LOTRBiomeVariant) biomeVariantField.get(variantBucket);
                    allVariants.add(biomeVariant);
                }
            }
        }
        catch(ReflectiveOperationException e) {
            log.fatal("Error getting biome variants for " + biome.biomeName);
            e.printStackTrace();
        }
        
        return allVariants;
    }
}
