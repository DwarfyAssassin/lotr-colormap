package io.gitlab.dwarfyassassin;

import net.minecraft.init.Blocks;
import net.minecraft.util.MathHelper;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.ModContainer;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import lotr.common.LOTRDimension;
import lotr.common.command.LOTRCommandTimeVanilla;
import lotr.common.util.LOTRLog;
import lotr.common.world.biome.LOTRBiome;

@Mod(modid = LOTRColorMap.MODID, version = LOTRColorMap.VERSION, dependencies = "required-after:lotr")
public class LOTRColorMap
{
    public static final String MODID = "lotrcolormap";
    public static final String VERSION = "1.1.0";
    
    private static Logger log;
    @Instance(value=MODID)
    private static LOTRColorMap instance;
    public static ColorMap colorMap;
    
    @EventHandler
    public void preInit (FMLPreInitializationEvent event) {
        getLogger();
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event) {
        if(Loader.isModLoaded("lotr")) log.info("Found lotr mod installed.");
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        colorMap = new ColorMap();
        for(LOTRDimension dimension : LOTRDimension.values()) colorMap.generateColorMap(dimension);
    }
    
    /*
    @EventHandler
    public void onServerStarting(FMLServerStartingEvent event) {
        event.registerServerCommand(new ColorMapCommand());
    }
    */
    
    public static ModContainer getModContainer() {
        return FMLCommonHandler.instance().findContainerFor(instance);
    }
    
    public static Logger getLogger() {
        return log == null ? (log = LogManager.getLogger("ColorMap")) : log;
    }
}
