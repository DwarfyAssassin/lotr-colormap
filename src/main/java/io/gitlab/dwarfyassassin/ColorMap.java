package io.gitlab.dwarfyassassin;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.logging.log4j.Logger;

import cpw.mods.fml.common.FMLCommonHandler;
import lotr.common.LOTRDimension;
import lotr.common.world.biome.LOTRBiome;
import lotr.common.world.biome.variant.LOTRBiomeVariant;
import lotr.common.world.biome.variant.LOTRBiomeVariantList;
import net.minecraft.client.Minecraft;
import net.minecraft.util.MathHelper;

public class ColorMap {
    private final Logger log;
    private final int colorMapImgSize = 256;
    private File colorMapFolder;
    
    public ColorMap() {
        log = LOTRColorMap.getLogger();
        
        resetFolder();
    }
    
    public void generateColorMap(LOTRDimension dimension) {
        try {
            BufferedImage img = new BufferedImage(colorMapImgSize, colorMapImgSize, BufferedImage.TYPE_INT_ARGB);
            File txtFile = new File(colorMapFolder, "Colormap " + dimension.dimensionName + ".txt");
            BufferedWriter txt = new BufferedWriter(new FileWriter(txtFile));

            txt.write("Color map for " + dimension.dimensionName + "\n");
            txt.write("Format:\n");
            txt.write("biomeID - biomeName - start x, start y - end x, end y\n");
            txt.write("  variantName - start x, start y - end x, end y\n\n");
        
            List<LOTRBiome> biomes = ColorMapUtils.removeNullFromArray(dimension.biomeList);
            int counter = 0;
            for(LOTRBiome biome : biomes) {
                counter++;
                
                log.info("Calculating the coordinates of " + biome.biomeName + " (" + counter + "/" + biomes.size() + ")");
                
                Triple<Set<Pair<Integer, Integer>>, Pair<Integer, Integer>, Pair<Integer, Integer>> coordinatesTriple = getImgCoordinates(biome);
                Set<Pair<Integer, Integer>> coordinates = coordinatesTriple.getLeft();
                Pair<Integer, Integer> startPoint = coordinatesTriple.getMiddle();
                Pair<Integer, Integer> endPoint = coordinatesTriple.getRight();
                
                for(Pair<Integer, Integer> coordinate : coordinates)  {
                    //log.debug("Image x: " + coordinate.getLeft() + " - Image y: " + coordinate.getRight());
                    img.setRGB(coordinate.getLeft(), coordinate.getRight(), biome.color);
                }
                
                txt.write(biome.biomeID + " - " + biome.biomeName + " - " + startPoint.getLeft() + ", " + startPoint.getRight() + " - " + endPoint.getLeft() + ", " + endPoint.getRight()  + "\n");
                
                
                Set<LOTRBiomeVariant> variants = ColorMapUtils.getBiomeVariants(biome);
                for(LOTRBiomeVariant variant : variants) {
                    log.debug("Calculating the coordinates of " + biome.biomeName + " " + variant.variantName);
                    
                    coordinatesTriple = getImgCoordinates(biome, variant);
                    coordinates = coordinatesTriple.getLeft();
                    startPoint = coordinatesTriple.getMiddle();
                    endPoint = coordinatesTriple.getRight();
                    
                    for(Pair<Integer, Integer> coordinate : coordinates)  {
                        //log.debug("Image x: " + coordinate.getLeft() + " - Image y: " + coordinate.getRight());
                        img.setRGB(coordinate.getLeft(), coordinate.getRight(), biome.color);
                    }
                    
                    txt.write("  " + variant.variantName + " - " + startPoint.getLeft() + ", " + startPoint.getRight() + " - " + endPoint.getLeft() + ", " + endPoint.getRight()  + "\n");
                }
           }
        
            writeColorMapImage(img, dimension);
            txt.close();
        } 
        catch (IOException e) {
            log.fatal("Failed to make colormap txt file for " + dimension.dimensionName);
            e.printStackTrace();
            return;
        }
    }
    
    public void resetFolder() {
        File colorMapFolder = new File(LOTRColorMap.getModContainer().getSource().getParentFile().getParentFile(), "color-maps");
        if (colorMapFolder.exists()) {
            for (File file : colorMapFolder.listFiles()) file.delete();
            colorMapFolder.delete();
        }
        colorMapFolder.mkdir();
        
        this.colorMapFolder = colorMapFolder;
    }

    private Triple<Set<Pair<Integer, Integer>>, Pair<Integer, Integer>, Pair<Integer, Integer>> getImgCoordinates(LOTRBiome biome) {
        return getImgCoordinates(biome, null);
    }
    
    private Triple<Set<Pair<Integer, Integer>>, Pair<Integer, Integer>, Pair<Integer, Integer>> getImgCoordinates(LOTRBiome biome, LOTRBiomeVariant variant) {
        Set<Pair<Integer, Integer>> coordinates = new HashSet<Pair<Integer, Integer>>();
        Pair<Integer, Integer> startPoint = null;
        Pair<Integer, Integer> endPoint = null;
        
        for(int y = 0; y < 256; y++) {
            for(float f = 0.0f; f < 4.05f; f += 0.1f) {
                float temperature = 0;
                float rainfall = biome.rainfall;
                
                // getFloatTemperature without noise but with randomness of noise
                if (y > 64) {
                    temperature = biome.temperature - (f + (float) y - 64.0F) * 0.05F / 30.0F;
                }
                else {
                    temperature = biome.temperature;
                }

                // Add variant boost
                if(variant != null) {
                    temperature += variant.tempBoost;
                    rainfall = variant.rainBoost;
                }
                
                // Clamp it
                double clampedTemperature = (double) MathHelper.clamp_float(temperature, 0.0f, 1.0f);
                double clamptedRainfall = (double) MathHelper.clamp_float(rainfall, 0.0f, 1.0f);
            
                // Calculate the image coordinates
                clamptedRainfall *= clampedTemperature;
                int imgX = (int)((1.0D - clampedTemperature) * 255.0D);
                int imgY = (int)((1.0D - clamptedRainfall) * 255.0D);
        
                coordinates.add(Pair.of(imgX, imgY));
                if(y == 0 && f == 0.0f) startPoint = Pair.of(imgX,  imgY);
                else if(y == 255 && f == 0.0f) endPoint = Pair.of(imgX,  imgY);
            }
        }
        
        return Triple.of(coordinates, startPoint, endPoint);
    }
    
    private void writeColorMapImage(BufferedImage img, LOTRDimension dim) {
        File imgFile = new File(colorMapFolder, "Colormap " + dim.dimensionName + ".png");
        if(imgFile.exists()) imgFile.delete();
        
        try {
            ImageIO.write(img, "png", imgFile);
            System.out.println("Succesfully wrote img to: " + imgFile);
        } catch (IOException e) {
            System.out.println("Failed to write img to: " + imgFile);
            e.printStackTrace();
        }
    }
}
