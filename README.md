Colormaps can be used in texture packs to change leaf and grass colors. See [this video](https://youtu.be/IVOPrYhHdgg?t=131 "https://youtu.be/IVOPrYhHdgg?t=131") for more info.
This mods generates a colormap template for all the LOTR mod biomes.

### How to use
Put this mod in your minecraft mod folder (server or client) and start minecraft. Doing this will generate a `.png` and `.txt` file for each LOTR dimension in the `color-map` folder in your base minecraft directory.

The `.png` file will show all the used pixels which form lines. These lines are the color of the biome color on the LOTR map. There is a lot of overlap so you will probably not find all the biome colors.

The `.txt` file will list all the biomes with ID and name and give the start and end coordinates of the lines.

### Planned features
- Config to disable generation in startup
- Command to (re)generate the color maps for all or one dimension

### For developers
To build the mod you need to have the LOTRMod in the libs folder.